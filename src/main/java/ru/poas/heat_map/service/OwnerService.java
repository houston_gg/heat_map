package ru.poas.heat_map.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.poas.heat_map.dao.OwnerDao;
import ru.poas.heat_map.dto.OwnerDto;
import ru.poas.heat_map.exception.OwnerNotFoundException;
import ru.poas.heat_map.model.Owner;

@Service
public class OwnerService {

    private OwnerDao ownerDao;

    @Autowired
    public OwnerService(OwnerDao ownerDao) {
        this.ownerDao = ownerDao;
    }

    public OwnerDto getAuthUserCredentials() {
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new OwnerDto(getOwnerByLogin(userDetails.getUsername()));
    }

    private Owner getOwnerByLogin(String login) {
        return ownerDao.findByLogin(login)
                .orElseThrow(()-> new OwnerNotFoundException("Owner with login " + login + " not found."));
    }
}
