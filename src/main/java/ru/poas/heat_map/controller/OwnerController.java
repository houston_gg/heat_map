package ru.poas.heat_map.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.poas.heat_map.exception.OwnerNotFoundException;
import ru.poas.heat_map.service.OwnerService;

@RestController
@RequestMapping(value = "/owner")
public class OwnerController {

    private OwnerService ownerService;

    @Autowired
    public OwnerController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }

    @GetMapping("/login")
    public ResponseEntity loginOwner() {
        try {
           return new ResponseEntity<>(ownerService.getAuthUserCredentials(), HttpStatus.OK);
        }
        catch (OwnerNotFoundException e) {
           return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
    }

}
